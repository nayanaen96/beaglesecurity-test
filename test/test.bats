#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE=${DOCKER_IMAGE:="test/beagle-bitbucket-pipe"}

  echo "Building image..."
  docker build -t ${DOCKER_IMAGE}:test .
}

@test "Dummy test" {
    run docker run \
        -e APPLICATION_TOKEN=${APPLICATION_TOKEN} \
        -e ACCESS_TOKEN=${ACCESS_TOKEN} \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 0 ]
}

