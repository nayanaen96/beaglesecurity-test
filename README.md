# Bitbucket Pipelines Pipe: Beagle Security Test Trigger

This can be used to trigger beagle penetration testing from Bitbucket

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: beaglesecurity/beaglesecurity-test:1.1.0
  variables:
    ACCESS_TOKEN: "<string>"
    APPLICATION_TOKEN: "<string>"
    # DEBUG: "<boolean>" # Optional
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| ACCESS_TOKEN (*)      | Access token from Beagle, It is recommended to use a secure repository variable. (Set as repository variables) |
| APPLICATION_TOKEN (*) | Application token from Beagle, It is recommended to use a secure repository variable. (Set as repository variables)|
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Details

### What is Beagle

Beagle is an intelligent and holistic platform to make your applications hack-proof. The platform provides continuous and automated Penetration Testing (under human supervision) for organizations, so that they can always stay on top of the cyber threats

In short, Beagle finds out how deep your system can be penetrated. Know it before the hackers do!

- [Beagle Security](https://beaglesecurity.com/) - Visit for more Details!

## Prerequisites

- Obtain application token and user token from Beagle dashboard.
- Add the repository variables "ACCESS_TOKEN" and "APPLICATION_TOKEN" to Bitbucket Pipelines.

### Generate your User Token From Beagle User Settings

  Settings -> Access token -> Generate your new personal access token

![Generate user token](https://beagle-assets.s3.ca-central-1.amazonaws.com/share/usertoken.png)

### Generate your Application Token From Beagle

  Home -> Applications -> Select your application -> Settings -> Application token

![Get application token](https://beagle-assets.s3.ca-central-1.amazonaws.com/share/apptoken.png)

## Examples

Basic example:

```yaml
- pipe: beaglesecurity/beaglesecurity-test:1.1.0
  variables:
    ACCESS_TOKEN: $ACCESS_TOKEN
    APPLICATION_TOKEN: $APPLICATION_TOKEN
```

## Support

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

[Rise issues and feature requests Here](https://bitbucket.org/beaglesecurity/beaglesecurity-test/issues)

For any support email to `info@beaglesecurity.com`
