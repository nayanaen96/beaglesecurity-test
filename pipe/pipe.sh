#!/usr/bin/env bash
#
# This can be used to trigger beagle penetration testing from Bitbucket
#

source "$(dirname "$0")/common.sh"

info "Executing the pipe..."

enable_debug() {
  if [[ "${DEBUG}" == "true" ]]; then
    info "Enabling debug mode."
    set -x
  fi
}
enable_debug

# Required parameters
APPLICATION_TOKEN=${APPLICATION_TOKEN:?'Application Token variable missing.'}
ACCESS_TOKEN=${ACCESS_TOKEN:?'Access Token variable missing.'}

# Default parameters
DEBUG=${DEBUG:="false"}

URL="https://api.beaglesecurity.com/v1/test/start/"

trigger_pentest() {
	curl --silent --header "Content-Type: application/json" --request POST --data "{\"access_token\":\"$ACCESS_TOKEN\",\"application_token\":\"$APPLICATION_TOKEN\"}" $URL | \
	sed -e 's/[{}]/''/g' | awk 'BEGIN { FS="\""; RS="," }; { if ($2 == "status") { print "var1="$4";"} if ($2 == "message") { print "var2="$4";"} }'
}

run trigger_pentest > vars.txt
tail  vars.txt | sed 's/=/=\"/g' | sed 's/;/\";/g' >  vars.txt
source vars.txt > /dev/null

info "Status: " $var1
info "Message:" $var2

if [[ "${status}" == "0" ]]; then
  success "Success!"
else
  fail "Error!"
fi
